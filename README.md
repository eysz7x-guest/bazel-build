# *http_archive*
*    **_name_** = [**_io_bazel_rules_go_**](https://github.com/bazelbuild/rules_go/releases/download/0.12.1/rules_go-0.12.1.tar.gz),
*    **_url_** = **_<https://github.com/bazelbuild/rules_go/releases/download/0.12.1/rules_go-0.12.1.tar.gz>_**,
*
<details>[**_`8b68d0630d63d95dacc0016c3bb4b76154fe34fca93efd65d1c366de3fcb4294 `_**](http://),<summary> **_`sha256`_**</summary>,

# *http_archive*
*    **_name_** = [**_bazel_gazelle_**](https://github.com/bazelbuild/bazel-gazelle/releases/download/0.12.0/bazel-gazelle-0.12.0.tar.gz),
*    **_url_** = **_<https://github.com/bazelbuild/bazel-gazelle/releases/download/0.12.0/bazel-gazelle-0.12.0.tar.gz>_**,
* <details>[**_`ddedc7aaeb61f2654d7d7d4fd7940052ea992ccdb031b8f9797ed143ac7e8d43`_**](http://),<summary> **_`sha256`_**</summary>

____
      load("@io_bazel_rules_go//go:def.bzl",
      "go_rules_dependencies",
      "go_register_toolchains")
      go_rules_dependencies()
      go_register_toolchains()
____
      load("@bazel_gazelle//:deps.bzl",
      "gazelle_dependencies",
      "go_repository")
      gazelle_dependencies()
____
# *`Go`[Repository](http://)*
*    [**_name_**](http://) = **_```com_github_stretchr_testify```_**,
*    **_```importpath```_** = [**_github.com/stretchr/testify_**](https://),
*    <details> [**f35b8ab0b5a2cef36673838d662e249dd9c94686d**](https://) <summary> **_[COMMIT](http://)_** 
</details></summary>
_______________